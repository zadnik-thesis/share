# share/tce/asm_dump #

Folder for memory dumps (diff files use the same location). Each folder
corresponds to a target being tested. They follow the following naming
convention: 'dump_name_Nexp' where 'name' is the tested target and 'Nexp' is
the FFT size as a power of 2 (N = 2^Nexp). (Diff files are the same but have
'diff' instead of 'dump'.)

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/share/tce/asm_dump
├── ag
├── out
├── outn
├── teemu_tf
├── tfg_k
├── tfg_tf
└── README.md
~~~~
