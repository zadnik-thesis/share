import warnings

import numpy as np

class OverflowWarning(Warning):
	pass

def dec2fixed(num, int_type='s'):
	"""Convert a number to a fixed point arithmetic.

	Fixed point format is Q15, two's complement.

	Parameters
	----------
	num : float
		Number to be fixed-pointed. Allowed range is [-1, 1).
	int_type : str
		Result integer type ('u' for unsigned, 's' for signed).

	Returns
	-------
	res : np.int16
		Result is 16-bit integer. In case of num being outside [-1, 1),
		the result saturates.
	"""

	allowed = ['u', 's']
	if int_type not in allowed:
		raise ValueError("Wrong integer type. Choose from {}.".format(allowed))

	if (num < -1) or (num >=1):
		warnings.warn("{} out of range [-1, 1).".format(num),
		              OverflowWarning)

	# Dirty fix for < -1
	if num <= -1:
		if int_type == 'u':
			return np.uint16(0x8000)
		elif int_type == 's':
			return np.int16(0x8000)

	abs_num = np.abs(num)
	res = 0
	for exp in range(1, 16):
		quant = 2**-exp
		if (quant <= abs_num):
			res |= (1 << (15-exp))
			abs_num -= quant
		if (abs_num <= 0):
			break
	# Two's complement
	if (num < 0):
		res = ~res + 1

	if int_type == 'u':
		return np.uint16(res)
	elif int_type == 's':
		return np.int16(res)

def fixed2dec(fixedin):
	"""Converts a fixed-point 'num' to a default float format.

	Assumes 'num' to be a 16-bit integer (signed or unsigned) with Q15 fixed
	point representation
	"""

	# Dirty fix for -1
	if (np.fabs(fixedin) == 0x8000):
		return -1

	if fixedin >> 16:
		warnings.warn("{:#x} is more than 16-bit.".format(fixedin),
		              OverflowWarning)

	num = np.int16(fixedin)
	if (num < 0):
		sign = -1
		#Two's complement
		num = ~num + 1
	else:
		sign = 1

	res = 0
	for exp in range(15, 0, -1):
		if (num >> (15-exp) & 1):
			res += 2**(-exp)

	return res*sign

def cplx2fixed(data):
	"""Converts complex 'data' (array or number) input to the same sized array
	of fixed points values.

	Output values are 32-bit. Higher 16 bits are an imaginary part (Q15), lower
	16 bits are a real part (Q15).
	"""

	data = np.array(data)
	out = np.zeros(data.shape, np.uint32)
	for x,y in np.nditer([data, out], op_flags=['readwrite']):
		re = np.uint16(dec2fixed(np.real(x)))
		im = np.uint16(dec2fixed(np.imag(x)))
		y[...] = (im << 16) + re
	return np.uint32(out)

def fixed2cplx(data):
	data = np.array(data)
	out = np.zeros(data.shape, np.complex)
	for x,y in np.nditer([data, out], op_flags=['readwrite']):
		fp_re = np.uint16(x & 0x0000ffff)
		fp_im = np.uint16((x & 0xffff0000) >> 16)
		re = fixed2dec(fp_re)
		im = fixed2dec(fp_im)
		y[...] = re + im*1j
	try:
		return np.complex(out)
	except:
		return out

def tst_dec_cross(step=2**(-15)):
	errs = []
	for dec_num in np.arange(-1, 1, step):
		print("Now testing: {}".format(dec_num), end='\r')
		fixed = dec2fixed(dec_num, 'u')
		res = fixed2dec(fixed)
		diff = dec_num - res
		if diff:
			errs.append({'reference'   : dec_num,
			             'computed'    : res,
			             'fixed-point' : fixed,
			             'difference'  : diff})
	print("\n{} errors".format(len(errs)))
	return errs

def tst_cplx_cross(step=2**(-7)):
	errs = []
	for re in np.arange(-1, 1, step):
		for im in np.arange(-1, 1, step):
			cplx = re + im*1j
			print("Now testing: {}".format(cplx), end='\r')
			fixed = cplx2fixed(cplx)
			res = fixed2cplx(fixed)
			diff = cplx - res
			if diff:
				errs.append({'reference'   : cplx,
				             'computed'    : res,
				             'fixed-point' : fixed,
				             'difference'  : diff})
	print("\n{} errors".format(len(errs)))
	return errs
