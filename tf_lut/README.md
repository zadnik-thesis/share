# share/tf_lut #

C header file containing definitions of constants used as a lookup table (LUT)
for twiddle factors in the C code (simulation of functional units in TCE). The
maximum FFT size is 16384 (2^14). The file contains two arrays of 2049 (N/8+1)
16-bit real parts and the same array of imaginary parts. The 16-bit value is
a fixed point Q15 representation.

[//]: # (tree)
## Directory tree ##
~~~~
/home/kubouch/git/thesis/share/tf_lut
├── README.md
└── tf_lut_16384.h  // THE Header
~~~~
